# -*- coding: utf-8 -*-

from flask.ext.script import Manager

from synopses import create_app
from synopses.extensions import db
from synopses.user import User, UserDetail, ADMIN, ACTIVE

app = create_app()
manager = Manager(app)

@manager.command
def run():
    """Run in local machine."""
    app.run(port=1895)


@manager.command
def initdb():
    """Init/reset database."""
    db.drop_all()
    db.create_all()

    admin = User(
            name=u'admin',
            email=u'admin@synopses.ch',
            password=u'123456',
            role_code=ADMIN,
            status_code=ACTIVE,
            user_detail=UserDetail(
                full_name = u'Dr. Justin Teim',
                specialty = u'Psychologist',
                bio = 'Lorem ipsum, mostly',
                phone = '079 123 4567'
            ))
    db.session.add(admin)
    db.session.commit()


manager.add_option('-c', '--config',
                   dest="config",
                   required=False,
                   help="config file")

if __name__ == "__main__":
    manager.run()
