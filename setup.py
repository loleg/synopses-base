# -*- coding: utf-8 -*-

from setuptools import setup

project = "synopses"

setup(
    name=project,
    version='0.1',
    url='https://bitbucket.com/loleg/synopses-base',
    description='eHealth applications to make doctors lives easier and their patients healthier',
    author='Oleg Lavrovsky',
    author_email='oleg@datalets.ch',
    packages=["synopses"],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'Flask>=0.10.1',
        'Flask-SQLAlchemy',
        'Flask-WTF',
        'Flask-Script',
        'Flask-Babel',
        'Flask-Testing',
        'Flask-Mail',
        'Flask-Cache',
        'Flask-Login',
        'Flask-OpenID',
        'nose',
        'mysql-python',
        'fabric',
    ],
    test_suite='tests',
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries'
    ]
)
