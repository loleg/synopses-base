from . import Patient, PatientContact, PatientRecord, EVENT

from ..extensions import db
from ..user import User

from datetime import datetime
from sqlalchemy import inspect
import json

contact_mapper = inspect(PatientContact)

def get_json_form(request_form):
    if request_form.keys()[0].startswith('{'):
        return json.loads(request_form.keys()[0])
    return request_form

def save_patient(formdata, user, patient_id=None):
    try:
        patient = Patient.query.filter_by(id=patient_id).first()
        if patient is None:
            patient = Patient()
            contact = PatientContact()
            patient.patient_contact = contact
            patient.hashit = Patient.generate_hash()
            patient.grant_access(user)
        else:
            contact = patient.patient_contact
            if not patient.user_has_access(user):
                msg = "Blocked user %d attempt to save patient %d"
                print msg % (user.id, patient_id)
                return False

        for col in contact_mapper.attrs:
            if (
                col.key is 'patient' or
                col.key is 'id' or
                '_time' in col.key
                ): continue
            if col.key in formdata:
                setattr(contact, col.key, formdata[col.key])

        if contact.first_name is "": raise ValueError("First name required")
        if contact.last_name is "": raise ValueError("Last name required")

        patient.name = "%s %s" % (contact.first_name, contact.last_name)
        patient.updated_time = datetime.utcnow()
    except ValueError as ex:
        return ex.message

    db.session.add(contact)
    db.session.add(patient)
    db.session.commit()

    # For importing data
    if 'records_all' in formdata:
        for r in formdata['records_all']:
            rdata = json.loads(r['data'])
            save_record(patient, user, r['type_code'], rdata)
    return True

def save_record(patient, user, record_type, formdata):
    try:
        record = PatientRecord()
        record.patient = patient
        record.user = user
        record.type_code = record_type

        if 'label' in formdata:
            record.label = formdata['label']
            formdata = {k:v for (k,v) in formdata.iteritems() if k != 'label'}
        record.data = json.dumps(formdata)

        patient.updated_time = datetime.utcnow()
        if record_type is EVENT:
            ds = "%sT%sZ" % (formdata['date'], formdata['time'])
            dt = datetime.strptime(ds, '%d.%m.%YT%H:%MZ')
            record.created_time = dt

    except ValueError as ex:
        return ex.message

    db.session.add(patient)
    db.session.add(record)
    db.session.commit()
    return True

def cache_record(record, jsondata):
    record.cache = json.dumps(jsondata)
    db.session.add(record)
    db.session.commit()
    return jsondata
