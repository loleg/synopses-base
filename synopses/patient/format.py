from .constants import *
from .validate import cache_record

import json

def record_threaded(self, TERMS_MED):
    if self.cache is not None and self.cache != "":
        return json.loads(self.cache)
    midata = json.loads(self.data)
    user_name = self.user.user_detail.full_name
    user_email = self.user.email
    label = self.label
    snippet = None
    body = ""
    icon = ""
    imageurl = ""
    if 'imageurl' in midata:
        imageurl = midata['imageurl']
    #attachments = append_attachments(midata)
    attachments = []
    if self.type_code == DATA:
        subject = "Medical record updated by %s" % user_name
        icon = "favorite"
        for m in midata:
            if midata[m]:
                body = "%s, %s: %s" % (body, m, midata[m])
        body = body.strip(', ')
        snippet = ""

    elif self.type_code == NOTE:
        if 'subject' in midata:
            subject = "%s -%s" % (midata['subject'], user_name)
        else:
            subject = "(Untitled)"
        body = midata['note']
        icon = "create"

    elif self.type_code == MED:
        if 'dosage' in midata:
            subject = midata['dosage']
        else:
            subject = midata['condition']
        subject = "Prescription record for %s by %s" % (subject, user_name)
        medis = [ midata["medication%d" % n] for n in range(1,3+1) ]
        body = ", ".join(medis).strip(', ')
        icon = "maps:local-pharmacy"

    elif self.type_code == LAB:
        if 'procedure' in midata: # TODO: clean up (use 'subject')
            subject = midata['procedure']
        else:
            subject = midata['condition']
        subject = "Lab report for %s" % subject
        body = midata['results']
        icon = "done"

    elif self.type_code == EVENT:
        subject = "Appointment with %s" % (midata['with'] or user_name)
        body = midata['note']
        icon = "event"

    elif self.type_code == SELFCHECK:
        subject = "Patient reports feeling '%s'" % midata['selfcheck']
        body = midata['note']
        icon = "accessibility"

    if snippet is None:
        snippet = (body[:75] + '..') if len(body) > 75 else body

    if len(body) > 5:
        for word in body.split():
            if word.lower() in TERMS_MED:
                body = body.replace(word, "{{%s}}" % word)

    return cache_record(self, {
        "snippet": "", "historyId": "",
        "messages": [{
            "id": self.id,
            "icon": icon,
            "imageurl": imageurl,
            "threadId": "", "historyId": "",
            "labelIds": [ "INBOX", "UNREAD" ],
            "snippet": snippet,
            "label": label,
            "payload": {
                "mimeType": "multipart/alternative",
                "filename": "",
                "headers": [{
                    "name": "MIME-Version", "value": "1.0"
                },{
                    "name": "Received",
                    "value": "by %s; %s" % (self.ip_address, self.created_time)
                },{
                    "name": "Date", "value": "%s" % self.created_time
                },{
                    "name": "Message-ID", "value": "<%d@med.synopses.ch>" % self.id
                },{
                    "name": "Subject", "value": subject
                },{
                    "name": "From", "value": "%s <%s>" % (user_name, user_email)
                },{
                    "name": "To", "value": "%s <%s>" % (self.patient.name, self.patient.contact.email)
                },{
                    "name": "Content-Type", "value": "multipart/alternative"
                }],
                "body": { "size": 0, "data": body },
                "parts": attachments
            },
            "sizeEstimate": 2000,
            "date": self.created_time.strftime('%d.%m.%Y'),
            "time": self.created_time.strftime('%H:%M'),
            "to": "%s <%s>" % (user_name, user_email),
            "subject": subject,
            "from": {
                "id": self.patient.id,
                "name": self.patient.name,
                "email": self.patient.contact.email
            },
            "unread": True,
            "starred": False
        }]
    })

def append_attachment(midata):
    attachments = []
    if 'textfile' in midata:
        attachments.append({
            "partId": "0",
            "mimeType": "text/plain",
            "filename": midata['textfile'],
            "headers": [{
                "name": "Content-Type",
                "value": "text/plain; charset=ISO-8859-1"
            },{
                "name": "Content-Transfer-Encoding",
                "value": "quoted-printable"
            }]
        })
    return attachments
