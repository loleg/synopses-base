# -*- coding: utf-8 -*-

# User status
INACTIVE = 0
NEW = 1
ACTIVE = 2
PATIENT_STATUS = {
    INACTIVE: 'inactive',
    NEW: 'new',
    ACTIVE: 'active',
}

# Record type
DATA = 0
NOTE = 1
LAB = 2
MED = 3
EVENT = 4
SELFCHECK = 5
RECORD_TYPE = {
    DATA: u'Data',
    NOTE: u'Notes',
    LAB: u'Results',
    MED: u'Medication',
    EVENT: u'Appointment',
    SELFCHECK: u'Patient note'
}

# Gender
MALE = 1
FEMALE = 2
OTHER = 9
SEX_TYPE = {
    MALE: u'Male',
    FEMALE: u'Female',
    OTHER: u'Other',
}
