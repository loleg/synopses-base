# -*- coding: utf-8 -*-

from sqlalchemy import Column, types, inspect, or_

from ..extensions import db
from ..utils import get_current_time, num_years, STRING_LEN, LONG_STRING_LEN
from ..user.models import User, DenormalizedText
from .constants import *

from os import urandom
from datetime import datetime
import binascii
import json

class PatientContact(db.Model):
    __tablename__ = 'patient_contact'
    id = Column(db.Integer, primary_key=True)
    updated_time = Column(db.DateTime, default=get_current_time)

    first_name = Column(db.String(STRING_LEN))
    last_name = Column(db.String(STRING_LEN))
    email = Column(db.String(LONG_STRING_LEN))
    phone1 = Column(db.String(STRING_LEN))
    phone2 = Column(db.String(STRING_LEN))
    address1 = Column(db.String(LONG_STRING_LEN))
    address2 = Column(db.String(LONG_STRING_LEN))
    city = Column(db.String(STRING_LEN))
    postal = Column(db.String(STRING_LEN))
    country = Column(db.String(STRING_LEN))
    insurance = Column(db.String(STRING_LEN))
    policyno = Column(db.String(STRING_LEN))
    socialsecurity = Column(db.String(STRING_LEN), default="")
    photo = Column(db.String(LONG_STRING_LEN), default="")
    sharednotes = Column(db.UnicodeText(), default=u"")

contact_mapper = inspect(PatientContact)

class Patient(db.Model):
    __tablename__ = 'patients'
    id = Column(db.Integer, primary_key=True)
    name = Column(db.String(STRING_LEN), nullable=False, unique=False)
    hashit = Column(db.String(32), nullable=False, unique=True)
    created_time = Column(db.DateTime, default=get_current_time)
    updated_time = Column(db.DateTime, default=get_current_time)

    patient_contact_id = Column(db.Integer, db.ForeignKey("patient_contact.id"))
    patient_contact = db.relationship("PatientContact", uselist=False, backref="patient")
    @property
    def contact(self):
        return self.patient_contact

    status_code = Column(db.SmallInteger, default=ACTIVE)
    @property
    def status(self):
        return PATIENT_STATUS[self.status_code]

    users_rwx = Column(DenormalizedText)
    def grant_access(self, user):
        if self.users_rwx is None: self.users_rwx = set()
        self.users_rwx.add(user.id)
    def revoke_access(self, user):
        if user.id in self.users_rwx:
            self.users_rwx.remove(user.id)
    def get_access_query(self):
        return User.query.filter(User.id.in_(self.users_rwx or set()))
    def get_user_details(self):
        return [u.user_detail.full_name for u in self.get_access_query()]
    def user_has_access(self, user):
        return user.id in self.users_rwx

    @classmethod
    def patients_rwx(cls, user_id):
        return cls.query.filter(
                or_(
                    cls.users_rwx == '%d' % user_id,
                    cls.users_rwx.like('%% %d' % user_id),
                    cls.users_rwx.like('%d %%' % user_id)
                )
            )

    def from_json(self, json):
        json_obj = json.loads(json)
        for k, v in json_obj.values():
            setattr(self, k, v)

    @property
    def data(self):
        def fdate(d):
            return d.strftime('%d.%m.%Y')
        profile = {
            'id':           self.id,
            'hash':         self.hashit,
            'name':         self.name,
            'updated':      fdate(self.updated_time),
            'record':       None,
            'photo':        self.contact.photo,
            'doctors':      self.get_user_details(),
            'initials': self.contact.first_name[0] + self.contact.last_name[0]
        }
        # Add contact records
        for col in contact_mapper.attrs:
            profile[col.key] = unicode(getattr(self.patient_contact, col.key))
        record = self.record_latest
        if record is not None:
            rd = json.loads(record.data)
            # Get simulated photo based on gender - remove in beta
            if 'sex_code' in rd and rd['sex_code'] != "":
                sex_code = int(rd['sex_code'])
                profile['gender'] = SEX_TYPE.get(sex_code)
            profile['record'] = rd
        if profile['hash'] is None:
            self.mash_hash()
            profile['hash'] = self.hashit

        return profile

    def fakephoto(self, gender=1):
        url = "https://randomuser.me/api/portraits/med/"
        gen = "men"
        if gender == 2: gen = "women"
        file = "/%d.jpg" % self.id
        return "".join([url, gen, file])

    def details(self):
        record = self.record_latest
        if record is None: return None
        rd = json.loads(record.data)

        if 'weight' in rd and rd['weight'] != "" and 'height' in rd and rd['height'] != "":
            height = float(rd['height'])
            rd['bmi'] = round(float(rd['weight']) / (height * 2), 1)

        if 'date_birth' in rd and rd['date_birth'] != "":
            rd['age'] = num_years(datetime.strptime(rd['date_birth'], '%d.%m.%Y'))

        cases = []
        counts = {}
        files = []

        for pr in self.records_all:
            data = json.loads(pr.data)
            if 'imageurl' in data and data['imageurl'] != "":
                if 'condition' in data:
                    pr_date = pr.created_time.strftime('%d.%m.%Y')
                    files.append("%s - %s" % (pr_date, data['condition']))
            if pr.label == "": continue
            if pr.label not in cases:
                cases.append(pr.label)
                counts[pr.label] = 0
            counts[pr.label] = counts[pr.label] + 1

        rd['cases'] = cases
        rd['cases_counts'] = counts
        rd['files'] = files

        return rd

    @property
    def records_all(self):
        return PatientRecord.query.filter_by(
            patient_id=self.id
        ).order_by(PatientRecord.created_time.desc())

    @property
    def records_all_as_data(self):
        return [r.as_data for r in self.records_all]

    @property
    def records_data(self):
        return PatientRecord.query.filter_by(
            type_code=DATA,
            patient_id=self.id
        ).order_by(PatientRecord.created_time.desc())

    @property
    def record_latest(self):
        return self.records_data.first()

    @classmethod
    def search(cls, keywords):
        criteria = []
        for keyword in keywords.split():
            keyword = '%' + keyword + '%'
            criteria.append(Patient.name.ilike(keyword))
        q = reduce(db.and_, criteria)
        return cls.query.filter(q)

    @classmethod
    def get_by_id(cls, patient_id):
        return cls.query.filter_by(id=patient_id).first_or_404()

    def check_name(self, name):
        return Patient.query.filter(
            db.and_(("%s %s" % Patient.first_name, Patient.last_name) == name)).count() == 0

    @classmethod
    def generate_hash(self):
        return binascii.hexlify(urandom(16))

    def mash_hash(self):
        self.hashit = Patient.generate_hash()
        db.session.add(self)
        db.session.commit()

class PatientRecord(db.Model):
    __tablename__ = 'patient_record'
    id = Column(db.Integer, primary_key=True)
    created_time = Column(db.DateTime, default=get_current_time)
    ip_address = Column(db.String(STRING_LEN), default="0.0.0.0")

    data = Column(db.UnicodeText(), nullable=False)
    label = Column(db.UnicodeText(), default=u"")
    cache = Column(db.UnicodeText(), default=u"")
    def clear_cache(self):
        self.cache = u""
        db.session.add(self)
        db.session.commit()

    type_code = db.Column(db.Integer)
    @property
    def type(self):
        return RECORD_TYPE.get(self.type_code)

    @property
    def as_data(self):
        return {
            'data': self.data,
            'label': self.label,
            'type_code': self.type_code
        }

    user_id = Column(db.Integer, db.ForeignKey("users.id"))
    user = db.relationship("User")
    patient_id = Column(db.Integer, db.ForeignKey("patients.id"))
    patient = db.relationship("Patient")
