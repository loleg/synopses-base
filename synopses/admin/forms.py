# -*- coding: utf-8 -*-

from flask.ext.wtf import Form
from wtforms import (
    HiddenField, SubmitField, RadioField, DateField,
    StringField, PasswordField, SelectMultipleField,
    TextField
)
from wtforms.validators import AnyOf, required, length

from ..user import USER_ROLE, USER_STATUS
from ..patient import PATIENT_STATUS

class UserForm(Form):
    next = HiddenField()
    name = StringField(u'Username', [required(), length(max=40)])
    email = StringField(u'E-mail', [required(), length(max=80)])
    avatar = StringField(u'Profile image')
    password = PasswordField(u'New password')
    role_code = RadioField(u"Role", [AnyOf([str(val) for val in USER_ROLE.keys()])],
            choices=[(str(val), label) for val, label in USER_ROLE.items()])
    status_code = RadioField(u"Status", [AnyOf([str(val) for val in USER_STATUS.keys()])],
            choices=[(str(val), label) for val, label in USER_STATUS.items()])
    #created_time = DateField(u'Created time')
    submit = SubmitField(u'Save')

class UserDetailForm(Form):
    next = HiddenField()
    full_name = StringField(u'Full name', [length(max=80)])
    specialty = StringField(u'Specialty', [length(max=80)])
    bio = TextField(u'Biography')
    phone = StringField(u'Telephone', [length(max=80)])
    submit = SubmitField(u'Save')

class PatientForm(Form):
    next = HiddenField()
    name = StringField(u'Name', [required(), length(max=40)])
    users_rwx = SelectMultipleField(u"Doctors", coerce=int)
    status_code = RadioField(u"Status", [AnyOf([str(val) for val in PATIENT_STATUS.keys()])],
            choices=[(str(val), label) for val, label in PATIENT_STATUS.items()])
    submit = SubmitField(u'Save')
