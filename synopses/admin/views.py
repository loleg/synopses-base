# -*- coding: utf-8 -*-

from flask import Blueprint, render_template, redirect, url_for, make_response, request, flash, jsonify
from flask.ext.login import login_required, current_user

from ..extensions import db
from ..decorators import admin_required

from ..user import User, UserDetail
from ..patient import Patient, save_patient
from .forms import UserForm, UserDetailForm, PatientForm

import json

admin = Blueprint('admin', __name__, url_prefix='/admin')


@admin.route('/')
@login_required
@admin_required
def index():
    users = User.query.all()
    return render_template('admin/index.html', users=users, active='index')


@admin.route('/users')
@login_required
@admin_required
def users():
    users = User.query.all()
    return render_template('admin/users.html', users=users, active='users')


@admin.route('/user/<int:user_id>', methods=['GET', 'POST'])
@login_required
@admin_required
def user(user_id):
    user = User.query.filter_by(id=user_id).first_or_404()
    form = UserForm(obj=user, next=request.args.get('next'))

    if form.validate_on_submit():
        form.populate_obj(user)

        db.session.add(user)
        db.session.commit()

        flash('User updated.', 'success')

    return render_template('admin/user.html', user=user, form=form)

@admin.route('/user/<int:user_id>/details', methods=['GET', 'POST'])
@login_required
@admin_required
def user_details(user_id):
    user = User.query.filter_by(id=user_id).first_or_404()
    userDetail = user.user_detail
    if userDetail is None:
        userDetail = UserDetail()
        user.user_detail = userDetail
        db.session.add(user)
        
    detailForm = UserDetailForm(obj=userDetail, next=request.args.get('next'))

    if detailForm.validate_on_submit():
        detailForm.populate_obj(userDetail)

        db.session.add(userDetail)
        db.session.commit()

        flash('User details updated.', 'success')

    return render_template('admin/userdetail.html', user=user, form=detailForm)

@admin.route('/user/new', methods=['GET', 'POST'])
@login_required
@admin_required
def user_new():
    user = User()
    form = UserForm(obj=user, next=request.args.get('next'))

    if form.validate_on_submit():
        form.populate_obj(user)

        db.session.add(user)
        db.session.commit()

        flash('User added.', 'success')

    return render_template('admin/usernew.html', form=form)

@admin.route('/patient/<int:patient_id>', methods=['GET', 'POST'])
@login_required
@admin_required
def patient(patient_id):
    patient = Patient.query.filter_by(id=patient_id).first_or_404()
    form = PatientForm(obj=patient, next=request.args.get('next'))

    all_users = [(u.id, u.name) for u in User.query.order_by('name')]
    form.users_rwx.choices = all_users

    if form.validate_on_submit():
        form.populate_obj(patient)

        db.session.add(patient)
        db.session.commit()

        flash('Patient updated.', 'success')

    return render_template('admin/patient.html', patient=patient, form=form)


@admin.route('/patients')
@login_required
@admin_required
def patients():
    patients = Patient.query.all()
    return render_template('admin/patients.html', patients=patients, active='patients')

@admin.route('/patients/import', methods=['POST'])
@login_required
@admin_required
def patient_import():
    file = request.files['file']
    filedata = file.read()
    jsondata = json.loads(filedata)
    count = 0
    for p in jsondata['patients']:
        if save_patient(p, current_user, p['id']):
            count = count + 1
    flash("%d imported/updated" % count, 'success')
    return redirect(url_for('admin.patients'))

@admin.route('/patients/export', methods=['GET'])
@login_required
@admin_required
def patient_export():
    patients = Patient.query.all()
    patientdata = []
    for p in patients:
        r = p.data
        r['records_all'] = p.records_all_as_data
        patientdata.append(r)
    jsondata = json.dumps({'patients': patientdata})
    response = make_response(jsondata)
    response.headers["Content-Disposition"] = "attachment; filename=patients.json"
    return response
