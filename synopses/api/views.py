# -*- coding: utf-8 -*-

from flask import (
    Blueprint, current_app, request, jsonify,
    send_from_directory, url_for
)
from flask.ext.login import login_user, current_user, logout_user
from ..decorators import requires_auth
from ..user import User
from ..patient import (
    Patient, PatientContact, PatientRecord,
    save_patient, save_record, record_threaded,
    constants, get_json_form
)
from ..utils import load_terms_med, get_current_time
from datetime import datetime, timedelta

import json
import os

api = Blueprint('api', __name__, url_prefix='/api')

from werkzeug import secure_filename

def allowed_file(filename):
    ALLOWED_EXTENSIONS = set([
        'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'tif', 'tiff'
    ])
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@api.route('/uploads/<path:fileurl>')
def uploaded_file(fileurl):
    return send_from_directory(current_app.config['UPLOAD_FOLDER'], fileurl)

def upload_attachment(foldername):
    file = request.files['file']
    if not file: return None
    if not allowed_file(file.filename): return -1

    filename = secure_filename(file.filename)
    filepath = os.path.join(current_app.config['UPLOAD_FOLDER'], foldername)
    try:
        os.mkdir(filepath)
    except OSError:
        True # already exists
    file.save(os.path.join(filepath, filename))
    fileurl = u"%s/%s" % (foldername, filename)
    # filespec = url_for('uploaded_file', fileurl=fileurl)
    filespec = u"/api/uploads/%s" % fileurl
    return filespec

def preload_static_data(app):
    """Load static data into memory (TODO: populate db)"""
    if hasattr(app, 'terms_med') and app.terms_med is not []: return
    app.terms_med = load_terms_med('wordlist-medicalterms-en.txt')
    app.terms_fda = load_terms_med('wordlist-druginfo-en.txt')
    app.terms_all = app.terms_med + app.terms_fda

@api.route('/login', methods=['POST'])
def login():
    preload_static_data(current_app)
    if current_user.is_authenticated():
        return jsonify(flag='success')
    username = request.form.get('username')
    password = request.form.get('password')
    if not username:
        jsondata = json.loads(request.form.keys()[0])
        if jsondata is not None:
            username = jsondata['username']
            password = jsondata['password']
    if username and password:
        user, authenticated = User.authenticate(username, password)
        if user and authenticated:
            if login_user(user, remember='y'):
                return jsonify(flag='success')
    current_app.logger.debug('login(api) failed, username: %s.' % username)
    return jsonify(flag='fail', msg='Sorry, try again.')

@api.route('/logout')
def logout():
    if current_user.is_authenticated():
        logout_user()
    return jsonify(flag='success', msg='Logouted.')

@api.route('/patients')
@requires_auth
def patients():
    patients = Patient.patients_rwx(current_user.id)
    patients = patients.order_by(Patient.updated_time.desc()).all()
    patientdata = [ p.data for p in patients ]
    showtime = ""
    for p in patientdata:
        if showtime == p['updated']:
            p['showtime'] = ""
        else:
            showtime = p['updated']
            if showtime == datetime.now().strftime('%d.%m.%Y'):
                p['showtime'] = "Today"
            elif showtime == (datetime.now() - timedelta(hours=24)).strftime('%d.%m.%Y'):
                p['showtime'] = "Yesterday"
            elif showtime == (datetime.now() + timedelta(hours=24)).strftime('%d.%m.%Y'):
                p['showtime'] = "Tomorrow"
            else:
                p['showtime'] = showtime
    return jsonify(patients=patientdata)

@api.route('/patient/<string:patient_hash>')
def patient_by_hash(patient_hash):
    # TODO: Patient authentication
    patient = Patient.query.filter_by(hashit=patient_hash).first()
    if patient is None:
        return jsonify(flag='fail', msg='Patient not found')
    return jsonify(patient=patient.data)

@api.route('/patient/<int:patient_id>/profile')
@requires_auth
def patient_profile(patient_id):
    patient = Patient.get_by_id(patient_id)
    if patient is None:
        return jsonify(flag='fail', msg='Patient not found')
    if not patient.user_has_access(current_user):
        return jsonify(flag='fail', msg='Access denied')
    if request.args.get('details'):
        return jsonify(details=patient.details())
    else:
        return jsonify(profile=patient.data)

@api.route('/patient/save', methods=['PUT', 'POST'])
@requires_auth
def patient_save():
    msg = save_patient(get_json_form(request.form), current_user)
    if msg is True:
        return jsonify(flag='success', msg='Patient saved')
    else:
        return jsonify(flag='fail', msg=msg)

@api.route('/patient/<int:patient_id>/upload', methods=['PUT', 'POST'])
@requires_auth
def patient_file_upload(patient_id):
    upl = None
    if request.files['file']:
        upl = upload_attachment("__%d_pic" % patient_id)
        if upl is None:
            return jsonify(flag='skip', msg='Attachment has failed')
        elif upl is -1:
            return jsonify(flag='fail', msg='Unsupported file type')
    msg = save_patient({ 'photo': upl }, current_user, patient_id)
    if msg is True:
        return jsonify(flag='success', msg='File saved', filespec=upl)

@api.route('/patient/<int:patient_id>/save', methods=['PUT', 'POST'])
@requires_auth
def patient_update(patient_id):
    msg = save_patient(get_json_form(request.form), current_user, patient_id)
    if msg is True:
        return jsonify(flag='success', msg='Patient saved')
    else:
        return jsonify(flag='fail', msg=msg)

@api.route('/threads')
@requires_auth
def threads():
    preload_static_data(current_app)
    # Filter by user
    patientset = (p.id for p in Patient.patients_rwx(current_user.id))
    records = PatientRecord.query.filter(PatientRecord.patient_id.in_(patientset)).order_by(PatientRecord.created_time.desc()).all()
    threads = [ record_threaded(r, current_app.terms_all) for r in records ]
    intoday = {
        'date': get_current_time().strftime('%d. %B'),
        'appointments': 0,
        'newmessages': 0,
        'labresults': 0
    }
    for r in records:
        if r.type_code == 2:
            intoday['labresults'] = intoday['labresults'] + 1
        if r.type_code == 5:
            intoday['newmessages'] = intoday['newmessages'] + 1
        if r.type_code == 4:
            intoday['appointments'] = intoday['appointments'] + 1
    return jsonify(threads=threads, intoday=intoday)

@api.route('/records/data')
@requires_auth
def records_data():
    records = PatientRecord.query.order_by(PatientRecord.created_time.desc()).all()
    return jsonify(records=[ r.data for r in records ])

@api.route('/records/clear_cache')
@requires_auth
def records_clear_cache():
    records = PatientRecord.query.filter(PatientRecord.cache!=u"").all()
    [ r.clear_cache() for r in records ]
    return jsonify(flag='success', msg='%d records cleared' % len(records))

@api.route('/patient/<int:patient_id>/threads')
@requires_auth
def patient_threads(patient_id):
    preload_static_data(current_app)
    patient = Patient.get_by_id(patient_id)
    if patient is None:
        return jsonify(flag='fail', msg='Patient not found')
    records = patient.records_all.all()
    return jsonify(threads=[ record_threaded(r, current_app.terms_all) for r in records ])

def save_patient_record(patient, record_type, record_data):
    if patient is None:
        return jsonify(flag='fail', msg='Patient not found')
    # print record_data
    try:
        msg = save_record(
            patient,
            current_user,
            record_type,
            record_data
        )
    except ValueError as e:
        return jsonify(flag='fail', msg=e.message)
    if msg is True:
        return jsonify(flag='success', msg='Record added')
    else:
        return jsonify(flag='fail', msg=msg)

@api.route('/patient/<string:patient_hash>/record/save', methods=['PUT', 'POST'])
def patient_hash_record_save(patient_hash):
    patient = Patient.query.filter_by(hashit=patient_hash).first()
    formdata = get_json_form(request.form)
    return save_patient_record(patient, constants.SELFCHECK, formdata)

@api.route('/patient/<string:patient_hash>/record/upload', methods=['PUT', 'POST'])
def patient_hash_record_upload(patient_hash):
    patient = Patient.query.filter_by(hashit=patient_hash).first()
    return patient_file_upload(patient.id)

@api.route('/patient/<int:patient_id>/record/<int:record_type>/save', methods=['PUT', 'POST'])
@requires_auth
def patient_id_record_save(patient_id, record_type):
    patient = Patient.get_by_id(patient_id)
    return save_patient_record(patient, record_type, get_json_form(request.form))

@api.route('/patient/<int:patient_id>/record/<int:record_type>/upload', methods=['PUT', 'POST'])
@requires_auth
def patient_id_record_upload(patient_id, record_type):
    return patient_file_upload(patient_id) #, record_type)

@api.route('/assist/medical/<string:query>')
def terms_medical(query):
    preload_static_data(current_app)
    matches = [term for term in current_app.terms_med+[''] if term.startswith(query)]
    return jsonify(matches=matches[:25])

@api.route('/assist/druginfo/<string:query>')
def terms_druginfo(query):
    preload_static_data(current_app)
    matches = [term for term in current_app.terms_fda+[''] if term.startswith(query)]
    return jsonify(matches=matches[:25])

# TODO: patient specific
@api.route('/assist/cases')
def user_cases():
    cases = []
    counts = {}
    # TODO: optimise (via distinct query?)
    query = PatientRecord.query.order_by(PatientRecord.created_time.desc()).all()
    for pr in query:
        if pr.label == "": continue
        if pr.label not in cases:
            cases.append(pr.label)
            counts[pr.label] = 0
        counts[pr.label] = counts[pr.label] + 1
    return jsonify(cases=[{ 'label': c, 'count': counts[c] } for c in cases])

@api.route('/users/list')
@requires_auth
def user_list():
    users = []
    me = None
    for u in User.query.filter(User.status_code>0).order_by(User.name.asc()).all():
        user = {
            'mailto': "mailto:%s" % u.email,
            'email': u.email,
            'avatar': u.avatar
        }
        if u.user_detail is None:
            user['name'] = u.name
        else:
            user['name'] = u.user_detail.full_name
        if u.id == current_user.id:
            me = user
        else:
            users.append(user)
    return jsonify(users=users, me=me)
