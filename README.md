# Synopses Server

Medical professionals are asking for fast, searchable, context-aware visibility into health profiles, while patients like us wish to better understand medical knowledge, and share it in confidence. We are a startup that develops platforms for personal access to medical history and doctor-patient communication connected to open health data sources.

This is a **PROTOTYPE** (not production-ready)  backend for an electronic patient record management system, based on Python / Flask / SQLAlchemy.

See also frontend at: https://bitbucket.org/loleg/synopses-app/

### Deployment

The framework for this backend server includes

- [SQLAlchemy](http://www.sqlalchemy.org)
- [WTForms](http://wtforms.simplecodes.com/)
- [Flask-Login](https://github.com/maxcountryman/flask-login)
- [Flask-Mail](http://packages.python.org/Flask-Mail/)
- [Flask-Testing](http://packages.python.org/Flask-Testing/)
- [Flask-Script](http://flask-script.readthedocs.org/en/latest/)
- [Flask-Babel](http://packages.python.org/Flask-Babel/)
- [mod\_wsgi](http://flask.pocoo.org/docs/deploying/mod_wsgi/) and [fabric](http://flask.pocoo.org/docs/patterns/fabric/)
- [HTML5 Boilerplate](https://github.com/h5bp/html5-boilerplate)
- [jQuery](http://jquery.com/)
- [Twitter Bootstrap](https://github.com/twitter/bootstrap)

## Installation

0) Prerequisites

- git
- pip
- fabric
- sqlite
- virtualenv
- nginx with WSGI support
- mysqllib:

  sudo apt-get install libmysqlclient-dev

1) Set up environment

`fab setup`

2) Activate the environment

`. venv/bin/activate`

3) Set up the configuration

`cp synopses/config.default synopses/config.py`

4) Start debugger

`fab d`

Open `http://127.0.0.1:5000` in your browser.

The database will be cleared each time in this mode. Create a new user.

## Deployment (WSGI)

Set up permissions

```
sudo chown `whoami` -R synopses
```

Set up virtual host

`TBD`

Install

`fab setup`

Configure

- Change `INSTANCE_FOLDER_PATH` in `utils.py`.
- Put `production.cfg` under `INSTANCE_FOLDER_PATH`.

## Project outline

    ├── app.wsgi                (mod_wsgi wsgi config)
    ├── CHANGES
    ├── fabfile.py              (fabric file)
    ├── synopses                (main app)
    │   ├── api                 (api module)
    │   ├── app.py              (create flask app)
    │   ├── config.py           (config module)
    │   ├── decorators.py
    │   ├── extensions.py       (init flask extensions)
    │   ├── frontend            (frontend module)
    │   ├── __init__.py
    │   ├── settings            (settings module)
    │   ├── static
    │   │   ├── css
    │   │   ├── favicon.png
    │   │   ├── humans.txt
    │   │   ├── img
    │   │   ├── js
    │   │   └── robots.txt
    │   ├── templates
    │   │   ├── errors
    │   │   ├── frontend
    │   │   ├── index.html
    │   │   ├── layouts
    │   │   ├── macros
    │   │   ├── settings
    │   │   └── user
    │   ├── translations        (i18n)
    │   ├── user                (user module)
    │   │   ├── constants.py
    │   │   ├── forms.py        (wtforms)
    │   │   ├── __init__.py
    │   │   ├── models.py
    │   │   ├── views.py
    │   ├── utils.py
    ├── LICENSE
    ├── manage.py               (manage via flask-script)
    ├── MANIFEST.in
    ├── README.markdown
    ├── screenshots
    ├── setup.py
    └── tests                   (unit tests, run via `nosetest`)

## License

See [LICENSE](LICENSE) file.

## Acknowledgement

Thanks to Python, Flask, its [extensions](http://flask.pocoo.org/extensions/), to [Wilson Xu](https://github.com/imwilsonxu) and community behind all dependencies of the open source platform.